# Twigger

Simple, structured PHP Twig renderer.
The running app will be available via `localhost:8080`.


# Usage

URL structure is the following:
`http://localhost:8080/{TEMPLATE}/{PAGE}`

This will load the `{PAGE}.twig` template inside from the `{TEMPLATE}` directory under the `src/templates` folder with variables parsed from `variables.json`.

# Namespaces (Template Paths)

## @block
You can refer a block inside `{TEMPLATE}/block` folder by `@block/{BLOCK}.twig`. So you can include a block like `{% include '@block/{BLOCK}.twig' %}`.

# Installation

Build the Docker images:
```
docker-compose build
```

Run Docker Container:
```
docker-compose up
```


Create necessary directories if necessary:
```
mkdir -p ./cache/twig
mkdir ./vendor
```

