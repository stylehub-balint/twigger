<?php

// Define Directories
define('DIR_APP', realpath(__DIR__ . '../'));
define('DIR_VENDOR', realpath(DIR_APP . '../../vendor') . '/');
define('DIR_CACHE', realpath('/cache') . '/');
define('DIR_TEMPLATES', realpath(DIR_APP . '../templates') . '/');

// Composer Dependencies
require_once DIR_VENDOR . 'autoload.php';

// Query Params
$q = explode("/", filter_input(INPUT_GET, "q"));
// Define Template and Page
$template = $q[0];
$page = $q[1];
// Default Template: default
if (empty($template)) {
    $template = "default";
}
// Default Page: Home
if (empty($page)) {
    $page = "home";
}

// Template
define('DIR_TEMPLATE', DIR_TEMPLATES . $template . '/');
define('FILE_VARIABLES', DIR_TEMPLATE . 'variables.json');
define('FILE_TEMPLATE', DIR_TEMPLATE . "pages/$page.twig");

// Show Error if Template Directory is missing
if (!is_dir(DIR_TEMPLATE)) {
    exit("There is no such template called $template available.");
}

// Show Error if Template File is missing
if (!is_file(FILE_TEMPLATE)) {
    exit("There is no such page called $page available in $template template.");
}

// Template Variables: Load variables from JSON if available
$templateVariables = [];
if (is_file(FILE_VARIABLES)) {
    $templateVariables = json_decode(file_get_contents(FILE_VARIABLES), true);
}

// Twig Environment
$loader = new \Twig\Loader\FilesystemLoader(DIR_TEMPLATE);
$twig = new \Twig\Environment($loader, [
    // 'cache' => DIR_CACHE . 'twig',   // Enable Cache
    'cache' => false,                   // Disable Cache
]);

// Block Templates
$loader->addPath(DIR_TEMPLATE . "block/", 'block');

// Load Template
$twigTemplate = $twig->load("pages/$page.twig");

// Render Template with Variables
echo $twigTemplate->render($templateVariables);
